/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.onload.estudostream.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onload.estudostream.EstudoStream;
import com.onload.estudostream.models.Pessoa;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alan
 */
public class LeitorService {

    public List<Pessoa> findAll() {

        List<Pessoa> pessoas = new ArrayList<Pessoa>();

        try ( BufferedReader rd = new BufferedReader(new FileReader("PessoaJson.json"))) {
            StringBuilder strBuilder = new StringBuilder();
            String linha;
            while ((linha = rd.readLine()) != null) {
                strBuilder = strBuilder.append(linha);

            }
            pessoas = new Gson().fromJson(strBuilder.toString(), new TypeToken<List<Pessoa>>() {
            }.getType());


        } catch (FileNotFoundException ex) {
            System.err.println(String.format("Erro: s%", ex.getMessage()));
        } catch (IOException ex) {
            Logger.getLogger(EstudoStream.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pessoas;

    }

}
