/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.onload.estudostream;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.onload.estudostream.models.Pessoa;
import com.onload.estudostream.service.LeitorService;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 *
 * @author alan
 */
public class EstudoStream {

    public static void main(String[] args) {
        
        LeitorService lt = new LeitorService();
        
        
        List<Pessoa> pessoas =  lt.findAll().stream().filter(x -> x.getId() >= 50).toList();
        pessoas.forEach(x->{
            System.out.println(String.format("id: %d | nome: %s | sexo: %s",x.getId(),x.getNome(),x.getSexo()));
        });

    }
}


